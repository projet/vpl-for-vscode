# VPL

The VPL extension let you download, upload and evaluate the files required by a Moodle 'Virtual Programming Lab' (VPL) activity from the environment of Visual Studio Code. Of course, you can also edit and run locally these files and take advantage of VSCode for these tasks.

## Features

- simple setup using the web link provided with each VPL
- upload / download tasks managed by a simple button click
- activity treeview for remote evaluation result

## Release Notes

The latest release is still a prototype that has not yet been tested at scale

# Documentation

A given folder is expected to work with, at most, a single VPL. All the informations related to a VPL are stored in a file named `'.vscode/vpl.json'` in the associated folder. The purpose of the initialization is to create this file.

## Initialization of a VPL

First, open a folder of your choice, in which the VPL content will be downloaded. Then, in the VPL
activity view, you can now initialize your VPL activity. This requires for you to copy/paste the web
link provided at the end of each VPL description page:
![Initialization of a VPL activity](https://gricad-gitlab.univ-grenoble-alpes.fr/projet/vpl-for-vscode-documentation/-/raw/master/Initialize.png)

The initialization will download all the required files from the given VPL. It will
also create a `'.vscode/vpl.json'` file in your folder, which contains various informations related to the VPL:
![Configuration of your VPL](https://gricad-gitlab.univ-grenoble-alpes.fr/projet/vpl-for-vscode-documentation/-/raw/master/Config.png)

## Working on your VPL

It is now possible to work locally on the VPL files from within Visual Studio code:
- edit files, as you would do for any other local files:
  ![Editing the files required by the VPL](https://gricad-gitlab.univ-grenoble-alpes.fr/projet/vpl-for-vscode-documentation/-/raw/master/Edition.png)
- run the programs that you write in these files, just like you would do with any other local file.
And it is also possible to interact with the VPL server:
- open the VPL activity view:
  ![The VPL activity view, before any evaluation](https://gricad-gitlab.univ-grenoble-alpes.fr/projet/vpl-for-vscode-documentation/-/raw/master/Ready.png)
- evaluate your VPL as if you were using the moodle interface using the `'check'` button:
  ![The VPl activity view, once the evaluation has been performed](https://gricad-gitlab.univ-grenoble-alpes.fr/projet/vpl-for-vscode-documentation/-/raw/master/Evaluated.png)
  This will automatically save your files locally and upload them to the VPL server.
- upload your files to the VPL server using the `'push'` button.
- download the files from your last submission saved on the server using the `'pull'` button.
- reset the files to their original content as defined by the VPL.
- add files to your submission by adding them to the end of the files list in the
  `'.vscode/vpl.json'` file (do not change the order of the required files, this would break your configuration).
