// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import * as fs from 'fs';
import * as https from 'https';
import * as http from 'http';
import * as querystring from 'querystring';
import * as util from 'util';

class Noeud extends vscode.TreeItem {
	children: Noeud[];
	constructor(label: string, collapsible?: vscode.TreeItemCollapsibleState) {
		super(label, collapsible);
		this.children = [];
	}
	addChild(n: Noeud) {
		this.children.push(n);
	}
	getChildren() {
		return this.children;
	}
}

class Arbre implements vscode.TreeDataProvider<Noeud> {
	root: Noeud | undefined;
	pending: boolean;
	private _onDidChangeTreeData: vscode.EventEmitter<Noeud | undefined | null | void> = new vscode.EventEmitter<Noeud | undefined | null | void>();
	readonly onDidChangeTreeData: vscode.Event<Noeud | undefined | null | void> = this._onDidChangeTreeData.event;

	constructor() {
		this.root = undefined;
		this.pending = false;
	}
	setPending() {
		this.pending = true;
		this.refresh();
	}
	clrPending() {
		this.pending = false;
		this.refresh();
	}
	refresh() {
		this._onDidChangeTreeData.fire();
	}
	getTreeItem(element: Noeud): vscode.TreeItem {
		return element;
	}
	getChildren(element?: Noeud | undefined): Thenable<Noeud[]> {
		if (element) {
			return Promise.resolve(element.getChildren());
		} else {
			if (this.pending) {
				return Promise.resolve([new Noeud("Evaluation in progress...")]);
			} else {
				if (this.root) {
					return Promise.resolve(this.root.getChildren());
				} else {
					return Promise.resolve([new Noeud("VPL not yet evaluated")]);
				}
			}
		}
	}
	constructTree(vpl: any) {
		let isFirst = true;
		this.root = new Noeud("Root");
		for (let key of Object.keys(vpl).sort().reverse()) {
			consoleLog("Handling category : " + key);
			let text: string = vpl[key];
			if (text.length > 0) {
				if (!isFirst) {
					this.root.addChild(new Noeud(""));
				}
				isFirst = false;
				let category = new Noeud(key, vscode.TreeItemCollapsibleState.Expanded);
				this.root.addChild(category);
				let first = category;
				let second = category;
				text.split("\n").forEach((line: string) => {
					if (line.length > 0) {
						switch (line.charAt(0)) {
						case '-':
							first = new Noeud(line.substr(1), vscode.TreeItemCollapsibleState.Expanded);
							second = first;
							category.addChild(first);
							break;
						case '*':
							second = new Noeud(line.substr(1), vscode.TreeItemCollapsibleState.Expanded);
							first.addChild(second);
							break;
						case '=':
							second = first;
						default:
							second.addChild(new Noeud(line));
						}
					}
				});
			}
		}	
	}
}

// GLOBAL STATE
var vplTree = new Arbre();
var vplContext: vscode.ExtensionContext;
var debug = false;

function consoleLog(message: string) {
	if (debug) {
		console.log(message);
	}
}

function findVplFolder() {
	let folders = vscode.workspace.workspaceFolders;
	return folders?folders[folders.length-1]:undefined;
}

function vplConfigDir(folder: vscode.WorkspaceFolder) {
	return vscode.Uri.joinPath(folder.uri, ".vscode");
}

function vplConfigFile(folder: vscode.WorkspaceFolder) {
	return vscode.Uri.joinPath(vplConfigDir(folder), "vpl.json");
}

async function writeVplFile(vplConfig: any, file: vscode.Uri) {
	let content = JSON.stringify(vplConfig, null, "  ");
	return util.promisify(fs.writeFile)(file.fsPath, content)
	.then(() => {
		return Promise.resolve();
	}).catch((err) => {
		vscode.window.showErrorMessage("Cannot write VPL config : " + err);
		return Promise.reject();
	});

}

async function writeVplConfig(vplConfig: any, folder: vscode.WorkspaceFolder) {
	let dir = vplConfigDir(folder);
	let file = vplConfigFile(folder);
	if (!fs.existsSync(dir.fsPath)) {
		return util.promisify(fs.mkdir)(dir.fsPath)
		.then(() => {
			return writeVplFile(vplConfig, file);
		}).catch((err) => {
			vscode.window.showErrorMessage("Cannot make directory : " + err);
			return Promise.reject();
		});
	} else {
		return writeVplFile(vplConfig, file);
	}
}

function getVplConfigSilently() {
	let folder = findVplFolder();

	if (folder) {
		let configFile = vplConfigFile(folder);
		try {
			let data = fs.readFileSync(configFile.fsPath);
			let vplConfig = JSON.parse(data.toString());
			return { config: vplConfig, error: "" };
		} catch (err) {
			return { config: undefined, error: "Cannot read VPL configuration : " + err };
		}
	} else {
		return { config: undefined, error: "No folder is opened" };
	}
}

async function getVplConfig() {
	return new Promise<any>((resolve, reject) => {
		let result: any = getVplConfigSilently();
		if (result.config) {
			return resolve(result.config);
		} else {
			vscode.window.showErrorMessage(result.error);
			return reject(result.error);
		}
	});
}

function vplInit() {
	let folder:any = findVplFolder();
	let configFile = folder ? vplConfigFile(folder) : undefined;
	if (configFile && fs.existsSync(configFile.fsPath)) {
		vscode.window.showErrorMessage("VPL already initialised. If you want to restart initialization, please remove "
			+ configFile.fsPath);
	} else {
		if (folder) {
			vscode.window.showInputBox({prompt: "Please type in your webservice link"})
			.then((link: string | undefined) => {
				if (link) {
					let parts = link.split('://');
					let slash = parts[1].indexOf('/');
					let body = parts[1].substring(slash);
					let subparts = body.split('?');
					let vplConfig: any = {};
					vplConfig.protocol = parts[0];
					vplConfig.hostname = parts[1].substring(0, slash);
					vplConfig.path = subparts[0];
					subparts[1].split('&').forEach(element => {
						let tokens = element.split('=');
						if (tokens[0] === "wstoken") {
							vplConfig.personalToken = tokens[1];
						} else if (tokens[0] === "id") {
							vplConfig.vplId = tokens[1];
						}
					});
					writeVplConfig(vplConfig, folder)
					.then(() => {
						fetchFiles('open', 'files');
						vscode.window.registerTreeDataProvider('vplcontrol', vplTree);
					});
				}
			});
		} else {
			vscode.window.showErrorMessage("Please open a folder where to initialize your VPL");
		}
	}
}

async function vplOpenView() {
	return vplRequest('info')
	.then((description) => {
		let panel = vscode.window.createWebviewPanel('vplView',
		description.name, vscode.ViewColumn.Active);
		panel.webview.html = description.intro;
	});	
}

async function vplSave(quiet: boolean = false) {
	return getVplConfig()
	.then(async (config) => {
		return vscode.workspace.saveAll()
		.then(async () => {
			let parameters: any = {};
			let folder: any = findVplFolder();
			let i=0;
			if (config.files) {
				config.files.forEach((element: any) => {
					let path = vscode.Uri.joinPath(folder.uri, element).fsPath;
					let content = fs.readFileSync(path);
					parameters[`files[${i}][name]`] = element;
					parameters[`files[${i}][data]`] = content.toString();
					i++;
				});
			}
			consoleLog(JSON.stringify(parameters, null, "  "));

			return vplRequest('save', parameters)
			.then((answer) => {
				consoleLog("Files submitted to the server, answer: " + JSON.stringify(answer));
				if (answer?.exception) {
					let message:string = answer.message;
					vscode.window.showErrorMessage(message.replace(/&#039;/g, "'").replace(/<br[^>]*>/g, ". "));
				} else {
					if (!quiet) {
						vscode.window.showInformationMessage("Files saved to the VPL");
					}
					return config;
				}
			})
			.catch((err) => {
				vscode.window.showErrorMessage("Cannot save files to the VPL: " + err);
				return Promise.reject();
			});
		});
	});
}

function vplEvaluate() {
	vplTree.setPending();
	vplSave(true)
	.then((config) => {
		runEvaluation(500, config);
	});
}

function runEvaluation(timeout: any, config: any) {
	vplRequest('evaluate')
	.then((data) => {
		consoleLog(`Evaluate data (timeout ${timeout}): ` + JSON.stringify(data));
		setTimeout(() => {
			vplRequest('get_result')
			.then((vpl) => {
				consoleLog(JSON.stringify(vpl, null, "  "));
				if (vpl.compilation === "" && vpl.evaluation === "" && vpl.grade === "") {
					runEvaluation(timeout*2, config);
				} else {
					vplTree.constructTree(vpl);
					vplTree.clrPending();
					vplContext.workspaceState.update(JSON.stringify(config), data);
				}
			})
			.catch((err) => {
				vscode.window.showErrorMessage("Cannot get VPL result: " + err);
			});
		}, timeout);
	})
	.catch((err) => {
		vscode.window.showErrorMessage("Cannot evaluate: " + err);
	});
}

function vplReset() {
	fetchFiles('info', 'reqfiles')
	.then(() => {
		vscode.window.showInformationMessage("Reset completed");
	});
}

function vplRequest(command: string, parameters: any = {}) {
	return new Promise<any>((resolve, reject) => { 
		getVplConfig()
		.then((config) => {
			parameters.wstoken = config.personalToken;
			parameters.id = config.vplId;
			parameters.wsfunction = "mod_vpl_" + command;
			parameters.moodlewsrestformat = "json";

			const postData = querystring.stringify(parameters);
			//consoleLog("Post data: " + postData);
			const options={
				hostname: config.hostname,
				path: config.path,
				method:'POST',
				headers: {
					/* eslint-disable @typescript-eslint/naming-convention */
					'Content-Type': 'application/x-www-form-urlencoded',
					'Content-Length': Buffer.byteLength(postData)
				}
			};
			let handleResult = (res: http.IncomingMessage) =>
			{
				let chunks : string[] = [];
				if (res.statusCode === 200) {
					//consoleLog(`HEADERS: ${JSON.stringify(res.headers)}`);
					res.setEncoding('utf8');
					res.on('data', (chunk) => {
						chunks.push(chunk);
					});
					res.on('end', () => {
						let data = chunks.join('');
						consoleLog("Request " + command + " answer: " + data);
						let result;
						try {
							result = JSON.parse(data);
						} catch (err) {
							// Usually the server answer is either JSON or null, be we never know...
							result = null;
						}
						if (result && result.exception) {
							vscode.window.showErrorMessage("Cannot complete request: " + result.message);
							consoleLog("Cannot complete request, server answer: " + data);
							reject();
						} else {
							consoleLog("Request " + command + " succeeded");
							resolve(result);
						}
					});		  
				} else {
					reject(res);
				}
			};
			let req;
			if (config.protocol === "https") {
				req = https.request(options, handleResult);
			} else {
				req = http.request(options, handleResult);
			}
			req.on('error', (e) => {
				reject(e);
			});
				
			// Write data to request body
			req.write(postData);
			req.end();
		}).catch((err) => {
			if (err) {
				consoleLog("vplRequest failed: " + err);
			}
			reject();
		});
	});
}

async function fetchFiles(request: string, field: string) {
	return getVplConfig()
	.then (async (config) => {
		let vpl;
		try {
			vpl = await vplRequest(request);
			let folder:any = findVplFolder();
			let newfiles = vpl[field];
			let confiles: string[] = config.files?config.files:[];
			let conupdated = false;
			let overwriteAll = false;
			let keepAll = false;
			for (let file of newfiles) {
				let data: string = file.data;
				let name = vscode.Uri.joinPath(folder.uri, file.name).fsPath;
				let canWrite = overwriteAll;
				if (fs.existsSync(name) && !overwriteAll && !keepAll) {
					const repOverAll = "Overwrite all existing files ?";
					const repKeepAll = "Keep all existing files ?";
					let rep = await vscode.window.showQuickPick([
						"Overwrite " + name + " ?", "Keep " + name + " ?", repOverAll, repKeepAll],
						{canPickMany: false, ignoreFocusOut: true});
					if (rep?.charAt(0) === 'O') {
						canWrite = true;
						if (rep === repOverAll) {
							overwriteAll = true;
						}
					} else {
						keepAll = true;
					}
				} else {
					canWrite = !keepAll;
				}
				if (canWrite) {
					consoleLog("Writing file " + file.name + " (" + data.length + " bytes)");
					fs.writeFile(name, data, (err)=>{
						if (err) {
							vscode.window.showErrorMessage("Cannot write " + file.name + ": " + err);
						}
					});
					if (!confiles.find((value) => {
						return value === file.name;
					})) {
						confiles.push(file.name);
						conupdated = true;
					}
				}						
			}
			if (conupdated) {
				config.files = confiles;
				await writeVplConfig(config, folder);
			}
			return Promise.resolve();
		} catch(err) {
			if (err) {
				vscode.window.showErrorMessage("Cannot download VPL infos : " + err);
			}
			return Promise.reject();
		}
	});
}

function vplOpen() {
	fetchFiles('open', 'files')
	.then(() => {
		vscode.window.showInformationMessage("Download completed");
	});
}

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

	// Use the console to output diagnostic information (consoleLog) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	consoleLog('Extension "VPL" is now activated!');
	vplContext = context;
	let result:any = getVplConfigSilently();
	if (result.config) {
		consoleLog("Found VPL config :");
		consoleLog("VPL ID : " + result.config.vplId);
		consoleLog("Personal Token : " + result.config.personalToken);
		let data:string | undefined = vplContext.workspaceState.get(JSON.stringify(result.config));
		if (data) {
			try {
				let vpl = JSON.parse(data);
				vplTree.constructTree(vpl);
			} catch (err) {
				// Case where the stored vpl state is just invalid, we ignore it
			}
		}
		vscode.window.registerTreeDataProvider('vplcontrol', vplTree);	
	} else {
		consoleLog("No VPL config found, reason :");
		consoleLog(result.error);
	};

	// The command has been defined in the package.json file
	// Now provide the implementation of the command with registerCommand
	// The commandId parameter must match the command field in package.json
	let commands : { [key: string]:()=>void } = {
		init: vplInit,
		save: vplSave,
		evaluate: vplEvaluate,
		reset: vplReset,
		open: vplOpen,
		subject: vplOpenView
	};
	for (let command in commands) {
		let disposable = vscode.commands.registerCommand('vpl.'+command, commands[command]);
		context.subscriptions.push(disposable);
	}
}

// this method is called when your extension is deactivated
export function deactivate() {}
