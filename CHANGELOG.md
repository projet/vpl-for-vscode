# Change Log

## [Unreleased]
No upcoming changes

## [0.2.3] - 2021-01-29
### Fixed
- regression bug introduced in 0.2.1
- evaluation hanged when the evaluation doesn't report any comment

## [0.2.2] - 2021-01-26
### Fixed
- Permission error incorrectly reported as 'newfile not iterable'

## [0.2.0] - 2020-12-08
### Added
- Command to open a view of the VPL description

## [0.1.0] - 2020-12-08
Initial release
### Added
- VPL activity view
- VPL commands
  - initialize a VPL access
  - evaluate a VPL
  - save files to the VPL server
  - get last VPL submission
  - reset VPL files content
- Evaluation results treeview